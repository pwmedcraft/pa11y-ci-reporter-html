# Pa11y CI HTML Reporter

Pa11y CI HTML Reporter generates HTML-formatted report files from [Pa11y CI](https://www.npmjs.com/package/pa11y-ci) JSON output.

## Requirements

Pa11y CI HTML Reporter accept as input a file containing the JSON output from pa11y-ci (version 2.0.0 and subsequent), for example:

```
pa11y-ci --json > pa11y-ci-results.json
```

This file must be either UTF-8 (default for Linux and Windows PowerShell 6 and later) or UTF-16LE (default for Windows PowerShell 5.1) encoded.

## Installation

Install Pa11y CI HTML Reporter globally (as shown below), or locally, via [npm](https://www.npmjs.com/).

```
npm install -g pa11y-ci-reporter-html
```

## Usage

Pa11y CI HTML Reporter is intended to be run as a command line tool.  Unlike pa11y reporters, it is called separately from Pa11y CI and passed the JSON results to be converted.  If no options are specified, it looks for a source file name `pa11y-ci-results.json` in the current directory, and saves the resulting HTML report files to the `./pa11y-ci-report` directory (creating the directory if required).

```
Usage: pa11y-ci-reporter-html [options]

Options:
  -V, --version                  output the version number
  -s, --source <file>            the path to the pa11y-ci JSON input file (default: "./pa11y-ci-results.json")
  -d, --destination <directory>  the path to the output directory for HTML report files (default: "./pa11y-ci-report")
  --include-zero-issues          include detailed page reports for pages with no pa11y issues
  -h, --help                     output usage information
```

The destination directory will be created if it does not already exist.

The following is an example running pa11y-ci and generating HTML reports from the results (assuming you've installed both `pa11y-ci` and `pa11y-ci-reporter-html` globally):

```
pa11y-ci --json > pa11y-ci-results.json
pa11y-ci-reporter-html
```

## HTML Reports

Pa11y CI HTML Reporter generates the following HTML report files in the specified `output` directory:

- An `index.html` file displaying summary results for all analyzed URLs, with links to each URL report (if applicable).
  - If `pa11y-ci` generates an error when analyzing the page (e.g. timeout, DNS lookup), it will be displayed instead of the summary.
- A detailed HTML report file for each analyzed URL with no pa11y issues.  These reports are generated using the [pa11y-reporter-html](https://www.npmjs.com/package/pa11y-reporter-html) module, with file names generated from the URL.  By default, these report files are not generated for pages with no pa11y issues, but the `--include-zero-issues` option can be used to override this and include reports for all pages.

An example report from pa11y-ci analysis of several pages from the [W3C accessibility demo site](https://www.w3.org/WAI/demos/bad/) can be found [here](https://gitlab-ci-utils.gitlab.io/pa11y-ci-reporter-html/).
