'use strict';

/**
 * Pa11y CI HTML Reporter is a command line tool used to input a Pa11y CI
 * JSON report and create a summary HTML report for all pages analyzed,
 * with detailed reports for each page.
 *
 * @module pa11y-ci-reporter-html
 */

const logger = require('ci-logger');
const reporter = require('./lib/reporter-html');
const validate = require('./lib/data-validators');

/**
 * Generate all HTML reports from the specified pa11y-ci JSON report.
 *
 * @static
 * @param {string}  pa11yciResultsFileName       Pa11y CI JSON report to convert.
 * @param {string}  outputDir                    Output directory to save HTML reports.
 * @param {object}  [options]                    Options for report creation.
 * @param {boolean} [options.includeZeroIssues]  Include page reports for pages with zero issues.
 */
const generateHtmlReports = async (pa11yciResultsFileName, outputDir, options) => {
    try {
        validate.isString(pa11yciResultsFileName, 'Invalid source file name');
        validate.isString(outputDir, 'Invalid output directory path');

        reporter.ensureOutputDirectory(outputDir);
        const formattedResults = reporter.getReformattedPa11yCiResults(pa11yciResultsFileName);
        const pages = await reporter.generatePageHtmlReports(formattedResults.results, outputDir, options);
        reporter.generateSummaryHtmlReport({ date: new Date(), pages }, outputDir);
    }
    catch (err) {
        logger.log({ message: err.message, level: logger.levels.error, exitOnError: true, errorCode: 1 });
    }
};

module.exports = generateHtmlReports;
