# Changelog

## v3.0.2 (2021-11-11)

### Fixed

- Fixed CSS error in HTML summary report ([!107](https://gitlab.com/gitlab-ci-utils/pa11y-ci-reporter-html/-/merge_requests/107))
- Updated to latest dependencies
- Updated project linting rules, and refactored code as required (#67)

### Miscellaneous

- Minor optimizations to CI pipeline (#65, #66)

## v3.0.1 (2021-09-12)

### Fixed

- Updated to latest dependencies
- Fixed error messages in some unit tests to work with changes in Node 16.9 (#63)

### Miscellaneous

- Setup [renovate](https://docs.renovatebot.com/) for dependency updates (#59)

## v3.0.0 (2021-06-27)

### Changed

- BREAKING: Changed the default destination directory from the current working directory to `./pa11y-ci-report` to avoid creating potentially a large number of files in the current working directory. To revert to the previous behavior, run with the `-d ./` option. (#58)
- BREAKING: Deprecated support for Node 10, 13, and 15 since end-of-life. Compatible with all current and LTS releases (`^12.20.0 || ^14.15.0 || >=16.0.0`). (#30, #50)

### Fixed

- Updated to latest dependencies

### Miscellaneous

- Added visual regression tests of HTML report (#55)

## v2.1.4 (2021-05-15)

### Fixed

- Updated to latest dependencies, including resolving vulnerabilities
- Fixed error in invalid encoding test (#51)

### Miscellaneous

- Optimized published package to only include the minimum required files (#49)

## v2.1.3 (2021-03-27)

### Fixed

- Updated to latest dependencies, including resolving vulnerabilities

### Miscellaneous

- Updated CI pipeline to use standard NPM package collection (#47) and use `needs` instead of `dependencies` for efficiency (#48)

## v2.1.2 (2021-02-27)

### Fixed

- Updated to latest dependencies, including resolving vulnerabilities (#43)

### Miscellaneous

- Updated CI pipeline to Pagean 4.4 with pages served from a static server (#44), add new CSS linting rules (#45), and optimize for schedules (#46)

## v2.1.1 (2020-11-29)

### Fixed

- Updated to latest dependencies

### Miscellaneous

- Updated CI pipeline to leverage simplified include syntax in GitLab 13.6 (#41) and GitLab Releaser (#42)
- Updated documentation for all exposed functions for consistency (#40)

## v2.1.0 (2020-10-19)

### Changed

- Added capability to read UTF-16LE encoded pa11y-ci JSON results file, which is the default format when piping output to a file in PowerShell 5.1. (#38)

### Fixed

- Updated to latest dependencies, including resolving vulnerabilities

### Miscellaneous

- Extracted CLI test module to new npm module [bin-tester](https://www.npmjs.com/package/bin-tester) (#39)

## v2.0.3 (2020-08-30)

### Fixed

- Updated to latest dependencies, including resolving vulnerabilities

## v2.0.2 (2020-04-05)

### Fixed

- Updated to latest dependencies, including resolving vulnerabilities
- Updated to latest GitLab `license_scanning` job

## v2.0.1 (2020-03-22)

### Fixed

- Updated to latest dependencies, including resolving vulnerabilities

## v2.0.0 (2020-01-22)

### Changed

- BREAKING: Changed default behavior to only generate page reports for analyzed URLs with pa11y issues, with command line option to override and generate page reports for all URLs (#32)
- BREAKING: Removed Node v8 compatibility since end-of-life. Compatible with all current and LTS releases (`^10.13.0 || ^12.13.0 || >=13.0.0`) (#26, #30)

### Fixed

- Fixed line endings to resolve yarn installation issue (#33)
- Updated to latest dependencies

### Miscellaneous

- Updated project to use [eslint-plugin-sonarjs](https://www.npmjs.com/package/eslint-plugin-sonarjs) (#27)

## v1.2.4 (2019-11-24)

### Fixed

- Updated to latest dependencies to resolve vulnerabilities

## v1.2.3 (2019-11-17)

### Fixed

- Updated to latest dependencies to resolve vulnerabilities

## v1.2.2 (2019-11-03)

### Fixed

- Updated to latest dependencies

### Miscellaneous

- Updated CI pipeline to lint HTML and CSS
- Updated project to use custom eslint configuration module (#25)

## v1.2.1 (2019-10-20)

### Fixed

- Updated to latest dependencies

## v1.2.0 (2019-08-04)

### Changed

- Updated the summary report to better highlight pages with issues versus those with no issues.  Also added filtering options for pages with pa11y issues, pages with no issues, and pages that pa11y failed to analyze.  All cases are included in [the example report](https://gitlab-ci-utils.gitlab.io/pa11y-ci-reporter-html/)).  (#16)

## v1.1.0 (2019-07-14)

### Changed

- Fixed bug where errors in pa11y-ci results caused report generation to fail.  These errors are now included in the summary report (see [the example report](https://gitlab-ci-utils.gitlab.io/pa11y-ci-reporter-html/)).  (#14)
- Updated summary report to show full report URL on hover to better handle truncated long names (#15)
- Improved validation of user input, error handling, and logging (#5, #11, #12)
- Updated testing to include CLI integration tests (#8) and `pa11y-ci` analysis of the sample report (#17)

### Documentation

- Updated readme to note that destination directory will be created if it does not exist (#13)

## v1.0.1 (2019-06-16)

### Documentation

- Moved repository to a new group and updated example location.

## v1.0.0 (2019-06-02)

Initial release
