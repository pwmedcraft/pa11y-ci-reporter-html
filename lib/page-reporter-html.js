'use strict';

const fs = require('fs');
const path = require('path');
const pageReporter = require('pa11y-reporter-html');
const utils = require('./utils');

const savePageHtmlReport = async (pageResult, outputDir) => {
    const fileName = `${utils.getFileNameFromUrl(pageResult.pageUrl)}.html`;
    const outputFile = path.join(outputDir, fileName);
    const htmlReport = await pageReporter.results(pageResult);
    fs.writeFileSync(outputFile, htmlReport);
    return fileName;
};

const getTypeCount = (issues, type) => {
    return issues.filter((result) => result.type === type).length;
};

const getPageSummary = pageResult => ({
    url: pageResult.pageUrl,
    issues: {
        errors: getTypeCount(pageResult.issues, 'error'),
        warnings: getTypeCount(pageResult.issues, 'warning'),
        notices: getTypeCount(pageResult.issues, 'notice')
    }
});

module.exports.savePageHtmlReport = savePageHtmlReport;
module.exports.getPageSummary = getPageSummary;
