'use strict';

// Covers for all URL non-alphanumeric reserved/unreserved characters (for consistency) per
// https://developers.google.com/maps/documentation/urls/url-encoding
const getFileNameFromUrl = url => url.replace(/(^https?:\/\/|\.html$)/g, '').replace(/[_.~!*'();:@&=+$,/?%#[\]]+/g, '-').replace(/^-|-$/, '');

module.exports.getFileNameFromUrl = getFileNameFromUrl;
