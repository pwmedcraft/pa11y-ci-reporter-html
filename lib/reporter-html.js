'use strict';

const path = require('path');
const fs = require('fs');
const handlebars = require('handlebars');
const logger = require('ci-logger');
const chardet = require('chardet');
const formatter = require('./data-formatter');
const pageReporter = require('./page-reporter-html');
const validate = require('./data-validators');

const summaryReportTemplateName = 'index.handlebars';
const summaryReportFileName = 'index.html';

const getFileEncoding = (fileName) => {
    // chardet has 100% confidence with utf16le (due to the BOM), but is not
    // always accurate for utf8, so assume utf8 unless known to be utf16le
    return chardet.detectFileSync(fileName) === 'UTF-16LE' ? 'utf16le' : 'utf8';
};

const getReformattedPa11yCiResults = (fileName) => {
    try {
        const encoding = getFileEncoding(fileName);
        // Results need to be trimmed if reading utf16le
        const jsonResults = fs.readFileSync(fileName, encoding).trim();
        const results = JSON.parse(jsonResults);
        return formatter.getResultsForHtmlReport(results);
    }
    catch (err) {
        let message;
        if (err.message.match(/ENOENT/)) {
            message = `Error opening file '${fileName}' - ${err.message}`;
        }
        else if (err.message.match(/JSON/)) {
            message = `Error parsing JSON from file '${fileName}' - ${err.message}`;
        }
        else {
            message = `Error retrieving pa11y-ci results from file '${fileName}' - ${err.message}`;
        }
        logger.log({ message, level: logger.levels.error, exitOnError: true, errorCode: 1 });
    }
};

const generateSummaryHtmlReport = (summary, outputDir) => {
    const templateFile = path.resolve(__dirname, summaryReportTemplateName);
    const summaryReportTemplate = fs.readFileSync(templateFile, 'utf-8');
    const template = handlebars.compile(summaryReportTemplate);
    const summaryReport = template(summary);
    const outputFile = path.join(outputDir, summaryReportFileName);
    fs.writeFileSync(outputFile, summaryReport);
};

// Allow cognitive complexity less than 10
// eslint-disable-next-line sonarjs/cognitive-complexity
const generatePageHtmlReports = async (pageResults, outputDir, options) => {
    const pages = [];
    for (let i = 0; i < pageResults.length; i++) {
        const pageResult = pageResults[i];
        try {
            if (validate.isPageError(pageResult)) {
                pages.push({ url: pageResult.pageUrl, message: pageResult.issues[0].message });
            }
            else {
                const pageSummary = pageReporter.getPageSummary(pageResult);
                if ((options && options.includeZeroIssues) || pageResult.issues.length > 0) {
                    pageSummary.htmlReport = await pageReporter.savePageHtmlReport(pageResult, outputDir);
                }
                pages.push(pageSummary);
            }
        }
        catch (err) {
            logger.log({ message: `Error creating report for '${pageResult.pageUrl}' - ${err.message}`, level: logger.levels.warn });
        }
    }
    return pages;
};

const ensureOutputDirectory = (outputDir) => {
    if (!fs.existsSync(outputDir)) {
        fs.mkdirSync(outputDir, { recursive: true });
    }
};

module.exports.generatePageHtmlReports = generatePageHtmlReports;
module.exports.generateSummaryHtmlReport = generateSummaryHtmlReport;
module.exports.getReformattedPa11yCiResults = getReformattedPa11yCiResults;
module.exports.ensureOutputDirectory = ensureOutputDirectory;
