'use strict';

const isString = (value, msg) => {
    if (typeof value !== 'string') {
        throw new TypeError(msg);
    }
};

const isPageError = (pageResult) => {
    // Page errors have one issue, and that issue has only one key "message"
    if (pageResult.issues.length === 1) {
        const issue = pageResult.issues[0]; // eslint-disable-line prefer-destructuring -- less intuitive
        if (Object.keys(issue).length === 1 && issue.message) {
            return true;
        }
    }
    return false;
};

module.exports.isString = isString;
module.exports.isPageError = isPageError;
