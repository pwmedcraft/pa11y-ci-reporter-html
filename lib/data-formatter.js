'use strict';

const getResultsForHtmlReport = results => ({
    summary: {
        total: results.total,
        passes: results.passes,
        errors: results.errors
    },
    results: Object.keys(results.results).map((key) => ({ pageUrl: key, issues: results.results[key] }))
});

module.exports.getResultsForHtmlReport = getResultsForHtmlReport;
