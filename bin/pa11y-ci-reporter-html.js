#!/usr/bin/env node
'use strict';

const program = require('commander');
const reporter = require('../index');
const pkg = require('../package.json');

program.version(pkg.version)
    .option('-s, --source <file>', 'the path to the pa11y-ci JSON input file', './pa11y-ci-results.json')
    .option('-d, --destination <directory>', 'the path to the output directory for HTML report files', './pa11y-ci-report')
    // This option is not called '--include-no-issues' because of commander's special treatment of 'no'
    .option('--include-zero-issues', 'include detailed page reports for pages with no pa11y issues')
    .parse(process.argv);
const options = program.opts();

const reporterOptions = { includeZeroIssues: options.includeZeroIssues };
reporter(options.source, options.destination, reporterOptions);
