'use strict';

const fs = require('fs');
const path = require('path');
const pa11yPageReporter = require('pa11y-reporter-html');
const pageReporter = require('../lib/page-reporter-html');
const utils = require('../lib/utils');

const pageResultsFileName = 'page-result-test.json';
const invalidPageResultFileError = 'Cannot read prop';

const getResults = (fileName) => {
    return require(`./test-cases/${fileName}`);
};

// eslint-disable-next-line max-lines-per-function
describe('save page HTML report', () => {
    const outputDirDefault = './';

    afterEach(() => {
        jest.restoreAllMocks();
    });

    it('should get file name, generate page HTML report, save report, and return file name', () => {
        expect.assertions(5); // eslint-disable-line no-magic-numbers
        const results = getResults(pageResultsFileName);
        const result = results.results[0]; // eslint-disable-line prefer-destructuring -- less intuitive
        const fileNameBase = 'foo-bar';
        const fileName = `${fileNameBase}.html`;
        const outputFile = path.join(outputDirDefault, fileName);
        const htmlReport = 'This is a test';

        const utilsSpy = jest.spyOn(utils, 'getFileNameFromUrl').mockImplementation(() => fileNameBase);
        const pa11yPageReporterSpy = jest.spyOn(pa11yPageReporter, 'results')
            .mockImplementation(() => Promise.resolve(htmlReport));
        const fsSpy = jest.spyOn(fs, 'writeFileSync').mockImplementation(() => { });

        const pageReporterResult = pageReporter.savePageHtmlReport(result, outputDirDefault);

        return pageReporterResult.then((returnedFileName) => {
            expect(utilsSpy).toHaveBeenCalledWith(result.pageUrl);
            expect(pa11yPageReporterSpy).toHaveBeenCalledWith(result);
            expect(fsSpy).toHaveBeenCalledWith(outputFile, htmlReport);
            expect(returnedFileName).toStrictEqual(fileName);
            expect(outputFile).toMatch(new RegExp(`.*${fileName}`));
        });
    });

    it('should throw when passed an undefined page result', () => {
        expect.assertions(1);
        return expect(pageReporter.savePageHtmlReport(undefined, outputDirDefault)).rejects.toThrow(invalidPageResultFileError);
    });

    it('should throw when passed an empty page result', () => {
        expect.assertions(1);
        return expect(pageReporter.savePageHtmlReport({}, outputDirDefault)).rejects.toThrow(invalidPageResultFileError);
    });

    it('should throw when passed an invalid page result', () => {
        expect.assertions(1);
        const invalidPageResult = {
            url: 'foo.html',
            issues: [0, 1, 2] // eslint-disable-line no-magic-numbers
        };
        return expect(pageReporter.savePageHtmlReport(invalidPageResult, outputDirDefault)).rejects.toThrow(invalidPageResultFileError);
    });

    it('should throw when passed results with page errors', () => {
        expect.assertions(1);
        const failResults = getResults('page-result-test-page-errors.json');
        const failResult = failResults.results[0]; // eslint-disable-line prefer-destructuring -- less intuitive
        return expect(pageReporter.savePageHtmlReport(failResult, outputDirDefault)).rejects.toThrow(invalidPageResultFileError);
    });

    // eslint-disable-next-line require-await
    const testInvalidOutputDir = async (outputDir, error) => {
        const results = getResults(pageResultsFileName);
        const result = results.results[0]; // eslint-disable-line prefer-destructuring -- less intuitive
        return expect(pageReporter.savePageHtmlReport(result, outputDir)).rejects.toThrow(error);
    };

    it('should throw when passed an undefined output dir', () => {
        expect.assertions(1);
        testInvalidOutputDir(undefined, '"path" argument');
    });

    it('should throw when passed a non-string output dir', () => {
        expect.assertions(1);
        testInvalidOutputDir({}, '"path" argument');
    });
});

// eslint-disable-next-line max-lines-per-function
describe('get page summary', () => {
    const getPageSummary = (page) => {
        const results = getResults(pageResultsFileName);
        return pageReporter.getPageSummary(results.results[page]);
    };

    it('should return a properly formatted page summary', () => {
        expect.assertions(1);
        const summary = getPageSummary(0);
        expect(summary).toStrictEqual(
            expect.objectContaining({
                url: expect.anything(),
                issues: expect.objectContaining({
                    errors: expect.any(Number),
                    warnings: expect.any(Number),
                    notices: expect.any(Number)
                })
            })
        );
    });

    it('should return the correct summary for the first test page', () => {
        expect.assertions(1);
        const summary = JSON.stringify(getPageSummary(0));
        expect(summary).toMatchSnapshot();
    });

    it('should return the correct summary for the second test page', () => {
        expect.assertions(1);
        const summary = JSON.stringify(getPageSummary(1));
        expect(summary).toMatchSnapshot();
    });

    it('should throw when passed an undefined page result', () => {
        expect.assertions(1);
        expect(() => pageReporter.getPageSummary(undefined)).toThrow(invalidPageResultFileError);
    });

    it('should throw when passed an empty page result', () => {
        expect.assertions(1);
        expect(() => pageReporter.getPageSummary({})).toThrow(invalidPageResultFileError);
    });

    it('should throw when passed an invalid page result', () => {
        expect.assertions(1);
        const invalidPageResult = {
            url: 'foo.html',
            issues: [0, 1, 2] // eslint-disable-line no-magic-numbers
        };
        expect(() => pageReporter.getPageSummary({ invalidPageResult })).toThrow(invalidPageResultFileError);
    });
});
