'use strict';

const fs = require('fs');
const path = require('path');
const del = require('del');
const bin = require('bin-tester');
const utils = require('../lib/utils');

// eslint-disable-next-line max-lines-per-function
describe('bin', () => {
    const testDir = './tests/test-cases';
    const defaultDestDir = `${testDir}/pa11y-ci-report`;
    const defaultFileName = './test-cases/pa11y-ci-results.json';
    const testErrorFileName = './test-cases/pa11y-ci-results-E-mix-error.json';
    const testNoneFileName = './test-cases/pa11y-ci-results-E-no-error.json';
    const summaryReportFileName = 'index.html';

    // Read source file, transform URL list into HTML report names, and add summary.
    // Source file is relative to this file for require statement.
    const getExpectedFileList = (sourceFile, includeZeroIssues = false) => {
        const sourceData = require(sourceFile);
        const urls = Object.keys(sourceData.results);
        const files = [summaryReportFileName];
        for (const url of urls) {
            if (includeZeroIssues || sourceData.results[url].length > 0) {
                files.push(`${utils.getFileNameFromUrl(url)}.html`);
            }
        }
        return files;
    };

    afterEach(() => {
        // With del 5 only forward slashes are supported, so force posix-style paths
        del.sync([defaultDestDir, path.posix.join(testDir, '*.html')]);
    });

    // Execute CLI in an external process and return error/stdout/stderr.
    // This will generate the actual report files, which are cleared after each test.
    //   sourceFile - file to open to determine expected HTML report files
    //   destinationDir - the directory to check for the expected HTML report files
    //   cwd - the current working directory for execution of the CLI
    //   args - array of arguments to pass to the CLI function
    const runPassingCliTest = async (sourceFile, destinationDir, cwd, args) => {
        const expectedFiles = getExpectedFileList(sourceFile);
        const results = await bin(args, cwd);
        expect(results.code).toBe(0);
        expect(results.stderr).toBe('');
        for (const file of expectedFiles) {
            expect(fs.existsSync(path.join(destinationDir, file))).toBe(true);
        }
    };

    // Execute CLI in an external process and return error/stdout/stderr.
    //   cwd - the current working directory for execution of the CLI
    //   args - array of arguments to pass to the CLI function
    //   exitCode - the expected exit code
    //   errorMessage - the string/regex to match stderr
    const runFailingCliTest = async (cwd, args, exitCode, errorMessage) => {
        const results = await bin(args, cwd);
        expect(results.code).toStrictEqual(exitCode);
        expect(results.stderr).toMatch(`${errorMessage}`);
    };

    // Run in test dir so default file is found and output files are saved there
    it('should generate expected files with no arguments', async () => {
        expect.assertions(7); // eslint-disable-line no-magic-numbers
        await runPassingCliTest(defaultFileName, defaultDestDir, testDir, []);
    });

    // Run in test dir so output files are saved there, but with different source file
    it('should generate expected files with source file specified', async () => {
        expect.assertions(6); // eslint-disable-line no-magic-numbers
        await runPassingCliTest(testErrorFileName, defaultDestDir, testDir, ['-s', path.basename(testErrorFileName)]);
    });

    // Run in test dir so default file is found, and specify the local directory for output files
    it('should generate expected files with destination dir specified', async () => {
        expect.assertions(7); // eslint-disable-line no-magic-numbers
        await runPassingCliTest(defaultFileName, testDir, testDir, ['-d', '.']);
    });

    // Run in parent dir and specify the source file and destination directory (both relative to root)
    it('should generate expected files with source file and destination dir specified', async () => {
        expect.assertions(3); // eslint-disable-line no-magic-numbers
        await runPassingCliTest(testNoneFileName, testDir, '.', ['-s', path.join(testDir, path.basename(testNoneFileName)), '-d', testDir]);
    });

    // Run in root dir with no arguments so default file is not found and execution fails.
    it('should generate expected error code with invalid source file', async () => {
        expect.assertions(2); // eslint-disable-line no-magic-numbers
        await runFailingCliTest('./', [], 1, 'ENOENT');
    });
});
