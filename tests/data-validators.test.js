'use strict';

const validate = require('../lib/data-validators');

// eslint-disable-next-line max-lines-per-function
describe('is string', () => {
    it('should not throw if value is a valid string', () => {
        expect.assertions(1);
        const value = 'foo';
        const msg = 'not a string';
        expect(() => validate.isString(value, msg)).not.toThrow();
    });

    const msg = 'not a string';
    const validateThrowsError = (value) => {
        expect(() => validate.isString(value, msg)).toThrow(new TypeError(msg));
    };

    it('should throw if value is a number', () => {
        expect.assertions(1);
        validateThrowsError(12345); // eslint-disable-line no-magic-numbers
    });

    it('should throw if value is a boolean', () => {
        expect.assertions(1);
        validateThrowsError(true);
    });

    it('should throw if value is an object', () => {
        expect.assertions(1);
        validateThrowsError({ foo: 'bar' });
    });

    it('should throw if value is an array', () => {
        expect.assertions(1);
        validateThrowsError(['12345']);
    });

    it('should throw if value is undefined', () => {
        expect.assertions(1);
        validateThrowsError(undefined);
    });

    it('should throw if value is null', () => {
        expect.assertions(1);
        validateThrowsError(null);
    });
});

// eslint-disable-next-line max-lines-per-function
describe('is page error', () => {
    const getPageResult = () => ({
        pageUrl: 'test1.html',
        issues: [{
            code: 'WCAG2AA.Principle2.Guideline2_4.2_4_2.H25.2',
            context: '<title>Welcome to CityLights! [Inacces...</title>',
            message: 'Check that the title element describes the document.',
            type: 'notice',
            typeCode: 3,
            selector: 'html > head > title'
        }]
    });

    const getPageErrorResult = () => ({
        pageUrl: 'error.html',
        issues: [{
            message: 'Pa11y timed out (30000ms)'
        }]
    });


    it('should return false for results with one a11y issue', () => {
        expect.assertions(1);
        const result = getPageResult();
        expect(validate.isPageError(result)).toBe(false);
    });

    it('should return false for results with no issues', () => {
        expect.assertions(1);
        const result = getPageResult();
        result.issues = [];
        expect(validate.isPageError(result)).toBe(false);
    });

    it('should return false for results with multiple issues', () => {
        expect.assertions(1);
        const result = getPageResult();
        result.issues.push(result.issues[0]);
        expect(validate.isPageError(result)).toBe(false);
    });

    it('should return false for results with multiple issues (mixed types)', () => {
        expect.assertions(1);
        const result = getPageResult();
        const errorResult = getPageErrorResult();
        result.issues.push(errorResult.issues[0]);
        expect(validate.isPageError(result)).toBe(false);
    });

    it('should throw for results object with no issues parameter', () => {
        expect.assertions(1);
        const result = getPageResult();
        delete result.issues;
        expect(() => validate.isPageError(result)).toThrow('Cannot read prop');
    });

    it('should throw for an empty object', () => {
        expect.assertions(1);
        expect(() => validate.isPageError({})).toThrow('Cannot read prop');
    });

    it('should return true for nominal page error results', () => {
        expect.assertions(1);
        const result = getPageErrorResult();
        expect(validate.isPageError(result)).toBe(true);
    });

    it('should return false for an unknown single result', () => {
        expect.assertions(1);
        const result = getPageErrorResult();
        result.issues[0].type = 'error';
        expect(validate.isPageError(result)).toBe(false);
    });
});
