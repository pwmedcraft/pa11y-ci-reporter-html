'use strict';

const path = require('path');
const del = require('del');
const { toMatchImageSnapshot } = require('jest-image-snapshot');
const protocolify = require('protocolify');
const puppeteer = require('puppeteer');
const htmlReporter = require('..');

expect.extend({ toMatchImageSnapshot });

const imageCompareOptions = {
    comparisonMethod: 'pixelmatch',
    diffDirection: 'vertical',
    // Allow 1% variation because report date will be different, and some font
    // differences between OSs
    failureThreshold: 0.01,
    failureThresholdType: 'percent'
};

const chromeLaunchOptions = {
    defaultViewport: {
        width: 1024,
        height: 768
    }
};

// eslint-disable-next-line max-lines-per-function
describe('visual tests', () => {
    const testFolder = path.join('.', 'tests');
    // Use file with errors, warnings, and notices to render all
    const pa11yciFileName = path.join(testFolder, 'test-cases', 'pa11y-ci-results-EWN-all-error.json');
    const resultsFolder = path.join(testFolder, 'visual-test-reports');
    const summaryReport = 'index.html';

    let browser;

    beforeAll(async () => {
        // Generate HTML reports (only need summary)
        await htmlReporter(pa11yciFileName, resultsFolder);
        browser = await puppeteer.launch(chromeLaunchOptions);
    });

    afterAll(async () => {
        // Delete HTML reports generated for this test
        await del([resultsFolder]);
        await browser.close();
    });

    it('should generate a summary report that renders the same visually', async () => {
        expect.assertions(1);

        const reportUrl = protocolify(path.join(resultsFolder, summaryReport));
        const page = await browser.newPage();
        await page.goto(reportUrl, { waitUntil: 'load' });
        const screenshot = await page.screenshot({ fullPage: true });

        expect(screenshot).toMatchImageSnapshot(imageCompareOptions);
    });
});
