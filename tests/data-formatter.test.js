'use strict';

const formatter = require('../lib/data-formatter');

// eslint-disable-next-line max-lines-per-function
describe('data formatter', () => {
    const checkValidResultObject = (fileName) => {
        const results = require(fileName);
        const formattedResults = formatter.getResultsForHtmlReport(results);
        expect(formattedResults).toStrictEqual(
            expect.objectContaining({
                summary: {
                    total: expect.any(Number),
                    passes: expect.any(Number),
                    errors: expect.any(Number)
                },
                results: expect.arrayContaining([
                    expect.objectContaining({
                        pageUrl: expect.anything(),
                        issues: expect.arrayContaining([])
                    })
                ])
            })
        );
    };

    it('should return a valid results object for results with no errors', () => {
        expect.assertions(1);
        const resultsFileName = './test-cases/pa11y-ci-results-E-no-error.json';
        checkValidResultObject(resultsFileName);
    });

    it('should return a valid results object for results with a mix of errors', () => {
        expect.assertions(1);
        const resultsFileName = './test-cases/pa11y-ci-results-E-mix-error.json';
        checkValidResultObject(resultsFileName);
    });

    it('should return a valid results object for results with all errors', () => {
        expect.assertions(1);
        const resultsFileName = './test-cases/pa11y-ci-results-EWN-all-error.json';
        checkValidResultObject(resultsFileName);
    });

    it('should throw for non-existent results file', () => {
        expect.assertions(1);
        const resultsFileName = './test-cases/non-existent-results.json';
        expect(() => checkValidResultObject(resultsFileName)).toThrow('Cannot find');
    });

    it('should throw for empty results file', () => {
        expect.assertions(1);
        const resultsFileName = './test-cases/pa11y-ci-results-empty.json';
        expect(() => checkValidResultObject(resultsFileName)).toThrow('Unexpected end of JSON');
    });

    it('should throw for invalid results file', () => {
        expect.assertions(1);
        const resultsFileName = './test-cases/pa11y-ci-results-invalid.json';
        expect(() => checkValidResultObject(resultsFileName)).toThrow('Cannot convert undefined');
    });
});
