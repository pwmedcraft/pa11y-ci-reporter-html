'use strict';

const fs = require('fs');
const path = require('path');
const handlebars = require('handlebars');
const logger = require('ci-logger');
const reporter = require('../lib/reporter-html');
const formatter = require('../lib/data-formatter');
const pageReporter = require('../lib/page-reporter-html');

// eslint-disable-next-line max-lines-per-function
describe('get reformatted pa11y-ci results', () => {
    const checkFileEncoding = (fileNameBase, expectedEncoding) => {
        expect.assertions(1);
        const fileName = `tests/${fileNameBase}`;
        const fsSpy = jest.spyOn(fs, 'readFileSync');

        reporter.getReformattedPa11yCiResults(fileName);

        expect(fsSpy).toHaveBeenCalledWith(fileName, expectedEncoding);
    };

    const verifyLogErrorAndExit = (fileName) => {
        const loggerSpy = jest.spyOn(logger, 'log').mockImplementation(() => { });

        reporter.getReformattedPa11yCiResults(fileName);

        expect(loggerSpy).toHaveBeenCalledWith(expect.objectContaining({
            message: expect.any(String),
            level: logger.levels.error,
            exitOnError: true,
            errorCode: 1
        }));
    };

    afterEach(() => {
        jest.restoreAllMocks();
    });

    it('should determine proper encoding for a utf16le encoded file', () => {
        expect.hasAssertions();
        const fileNameBase = 'test-cases/pa11y-ci-results-E-no-error-utf16le.json';
        const encoding = 'utf16le';
        checkFileEncoding(fileNameBase, encoding);
    });

    it('should determine proper encoding for a utf8 encoded file', () => {
        expect.hasAssertions();
        const fileNameBase = 'test-cases/pa11y-ci-results-E-no-error.json';
        const encoding = 'utf8';
        checkFileEncoding(fileNameBase, encoding);
    });

    it('should should log error and exit when parsing pa11y-ci results encoded in a format other than uft8 or utf16le', () => {
        expect.hasAssertions();
        const fileName = 'tests/test-cases/pa11y-ci-results-E-mix-error-utf16be.json';
        verifyLogErrorAndExit(fileName);
    });

    it('should read a valid results file, reformat the data, and return those results', () => {
        expect.assertions(3); // eslint-disable-line no-magic-numbers
        const fileNameBase = 'test-cases/pa11y-ci-results-E-mix-error.json';
        const fileName = `tests/${fileNameBase}`;
        const results = require(`./${fileNameBase}`);
        const fsSpy = jest.spyOn(fs, 'readFileSync');
        const formatterSpy = jest.spyOn(formatter, 'getResultsForHtmlReport');

        const formattedResults = reporter.getReformattedPa11yCiResults(fileName);

        expect(fsSpy).toHaveBeenCalledWith(fileName, 'utf8');
        expect(formatterSpy).toHaveBeenCalledWith(results);
        expect(formattedResults).toMatchSnapshot();
    });

    it('should log error and exit when passed an undefined file name', () => {
        expect.assertions(1);
        verifyLogErrorAndExit(undefined);
    });

    it('should log error and exit when passed an empty file name', () => {
        expect.assertions(1);
        verifyLogErrorAndExit('');
    });

    it('should log error and exit when passed an invalid file name', () => {
        expect.assertions(1);
        verifyLogErrorAndExit('does-not-exist.json');
    });

    it('should log error and exit when passed a file name with invalid JSON', () => {
        expect.assertions(1);
        verifyLogErrorAndExit('tests/test-cases/invalid-json.json');
    });

    it('should log error and exit when passed a file name with invalid pa11y-ci JSON', () => {
        expect.assertions(1);
        verifyLogErrorAndExit('tests/test-cases/pa11y-ci-results-invalid.json');
    });
});

describe('generate summary HTML report', () => {
    const outputDir = './foo';

    afterEach(() => {
        jest.restoreAllMocks();
    });

    it('should read handlebars template, compile it, pass the supplied data, and save output', () => {
        expect.assertions(3); // eslint-disable-line no-magic-numbers
        const summaryData = require('./test-cases/summary-results.json');
        const htmlTemplate = '<html><body>foo</body></html>';
        const summaryReportFileName = path.join(outputDir, 'index.html');
        const getJson = (input) => JSON.stringify(input);

        // fsReadSpy returns an HTML template, which is passed to handlebars.compile
        const fsReadSpy = jest.spyOn(fs, 'readFileSync').mockImplementation(() => htmlTemplate);

        // handlebars.compile returns a function that will be passed the summary data, so in this case
        // simply return the stringified input data to confirm that's what's being saved with fs.writeFile
        const handlebarsSpy = jest.spyOn(handlebars, 'compile').mockImplementation(() => getJson);
        const fsWriteSpy = jest.spyOn(fs, 'writeFileSync').mockImplementation(() => { });

        reporter.generateSummaryHtmlReport(summaryData, outputDir);

        expect(fsReadSpy.mock.calls[0][0]).toMatch(/.*\.handlebars/);
        expect(handlebarsSpy).toHaveBeenCalledWith(htmlTemplate);
        expect(fsWriteSpy).toHaveBeenCalledWith(summaryReportFileName, getJson(summaryData));
    });
});

// eslint-disable-next-line max-lines-per-function
describe('generate page html reports', () => {
    afterEach(() => {
        jest.restoreAllMocks();
    });

    const outputDir = './foo';
    const summaryDataFileIncludeZero = './test-cases/summary-results-mix-page-errors-includezero.json';
    const summaryDataFileDefault = './test-cases/summary-results-mix-page-errors.json';

    const verifyPageHtmlReports = (options, summaryDataFile) => {
        const pageSummaryData = require('./test-cases/page-result-test-mix-page-errors.json');
        const summaryData = require(summaryDataFile);

        // For the given test case, the resulting HTML report is the same as the page URL
        const pageHtmlReportSpy = jest.spyOn(pageReporter, 'savePageHtmlReport').mockImplementation((page) => page.pageUrl);
        const pageSummarySpy = jest.spyOn(pageReporter, 'getPageSummary');

        const resultPromise = reporter.generatePageHtmlReports(pageSummaryData.results, outputDir, options);

        return resultPromise.then((result) => {
            const count = pageSummaryData.results.length;
            // The third url has no errors, so no report is generated and HTML report function  should be called one less time
            // The last url has a page error, so the page report functions should be called one less time
            const pageReportCount = options && options.includeZeroIssues ? count - 1 : count - 1 - 1;
            const pageSummaryCount = count - 1;
            expect(pageHtmlReportSpy).toHaveBeenCalledTimes(pageReportCount);
            expect(pageSummarySpy).toHaveBeenCalledTimes(pageSummaryCount);
            for (let i = 0; i < pageReportCount; i++) {
                expect(pageHtmlReportSpy).toHaveBeenNthCalledWith(i + 1, pageSummaryData.results[i], outputDir);
                expect(pageSummarySpy).toHaveBeenNthCalledWith(i + 1, pageSummaryData.results[i]);
            }
            // Check resulting data to confirm all urls, including page error, are included
            expect(result).toStrictEqual(summaryData.pages);
        });
    };

    it('should save HTML report, get summary for each page without page error, and return resulting summary for includeZeroIssues true', () => {
        expect.assertions(9); // eslint-disable-line no-magic-numbers
        const options = { includeZeroIssues: true };
        return verifyPageHtmlReports(options, summaryDataFileIncludeZero);
    });

    it('should save HTML report, get summary for each page with pa11y issues, and return resulting summary for includeZeroIssues false', () => {
        expect.assertions(7); // eslint-disable-line no-magic-numbers
        const options = { includeZeroIssues: false };
        return verifyPageHtmlReports(options, summaryDataFileDefault);
    });

    it('should save HTML report, get summary for each page with pa11y issues, and return resulting summary for includeZeroIssues undefined', () => {
        expect.assertions(7); // eslint-disable-line no-magic-numbers
        const options = { includeZeroIssues: undefined };
        return verifyPageHtmlReports(options, summaryDataFileDefault);
    });

    it('should save HTML report, get summary for each page with pa11y issues, and return resulting summary for options undefined', () => {
        expect.assertions(7); // eslint-disable-line no-magic-numbers
        const options = undefined;
        return verifyPageHtmlReports(options, summaryDataFileDefault);
    });

    it('should log error for invalid pa11y-ci results', () => {
        expect.assertions(6); // eslint-disable-line no-magic-numbers
        const options = { includeZeroIssues: false };
        // The case includes one url with invalid JSON for one error, so should fail
        const pageSummaryData = require('./test-cases/page-result-test-invalid-pa11yci.json');

        // Call function to generate report since it should fail, but mock fs.writeFileSync to confirm
        const loggerSpy = jest.spyOn(logger, 'log').mockImplementation(() => { });
        const fsSpy = jest.spyOn(fs, 'writeFileSync').mockImplementation(() => { });
        const pageHtmlReportSpy = jest.spyOn(pageReporter, 'savePageHtmlReport');
        const pageSummarySpy = jest.spyOn(pageReporter, 'getPageSummary').mockImplementation(() => { });

        const resultPromise = reporter.generatePageHtmlReports(pageSummaryData.results, outputDir, options);

        return resultPromise.then((result) => {
            expect(pageSummarySpy).toHaveBeenCalledWith(pageSummaryData.results[0]);
            // Expect savePageHtmlReport to have been called once with expected data, throw error during report generation,
            // should not try to write file, and should log warning
            expect(pageHtmlReportSpy).toHaveBeenCalledTimes(1);
            expect(pageHtmlReportSpy).toHaveBeenCalledWith(pageSummaryData.results[0], outputDir);
            expect(fsSpy).not.toHaveBeenCalled();
            expect(loggerSpy).toHaveBeenCalledWith(expect.objectContaining({
                message: expect.any(String),
                level: logger.levels.warn
            }));
            // Since only one url that had invalid data, should get empty results
            expect(result).toStrictEqual([]);
        });
    });
});

describe('ensure output directory', () => {
    afterEach(() => {
        jest.restoreAllMocks();
    });

    it('should create dir that does not exist', () => {
        expect.assertions(1);
        const outputDir = 'foo/';
        const fsMkdirSpy = jest.spyOn(fs, 'mkdirSync').mockImplementation(() => { });

        reporter.ensureOutputDirectory(outputDir);

        expect(fsMkdirSpy).toHaveBeenCalledWith(outputDir, expect.objectContaining({}));
    });

    it('should not create dir that does exist', () => {
        expect.assertions(1);
        const outputDir = './';
        const fsMkdirSpy = jest.spyOn(fs, 'mkdirSync').mockImplementation(() => { });

        reporter.ensureOutputDirectory(outputDir);

        expect(fsMkdirSpy).not.toHaveBeenCalled();
    });
});
