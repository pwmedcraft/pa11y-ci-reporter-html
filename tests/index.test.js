'use strict';

const logger = require('ci-logger');
const reporter = require('../lib/reporter-html');
const pa11yCiReporterHtml = require('../index');

// eslint-disable-next-line max-lines-per-function
describe('generate summary HTML report', () => {
    afterEach(() => {
        jest.restoreAllMocks();
    });

    it('should check output directory, get formatted results for given file name, generate page HTML report, and generate summary HTML report', () => {
        expect.assertions(4); // eslint-disable-line no-magic-numbers
        const pa11yciResultsFileName = './test-cases/pa11y-ci-results-EWN-all-error.json';
        const formattedResults = require('./test-cases/page-result-test.json');
        const pageSummary = [{ foo: 'bar' }];
        const outputDir = 'foo/';
        const options = { includeZeroIssues: false };

        const reporterEnsureOutputSpy = jest.spyOn(reporter, 'ensureOutputDirectory').mockImplementation(() => { });
        const reporterReformatSpy = jest.spyOn(reporter, 'getReformattedPa11yCiResults').mockImplementation(() => formattedResults);
        const reporterPageHtmlSpy = jest.spyOn(reporter, 'generatePageHtmlReports').mockImplementation(() => pageSummary);
        const reporterPageSummarySpy = jest.spyOn(reporter, 'generateSummaryHtmlReport').mockImplementation(() => { });

        const resultPromise = pa11yCiReporterHtml(pa11yciResultsFileName, outputDir, options);

        return resultPromise.then(() => {
            expect(reporterEnsureOutputSpy).toHaveBeenCalledWith(outputDir);
            expect(reporterReformatSpy).toHaveBeenCalledWith(pa11yciResultsFileName);
            expect(reporterPageHtmlSpy).toHaveBeenCalledWith(formattedResults.results, outputDir, options);
            expect(reporterPageSummarySpy).toHaveBeenCalledWith(expect.objectContaining({
                date: expect.any(Date),
                pages: pageSummary
            }), outputDir);
        });
    });

    const verifyLogErrorAndExit = (pa11yciResultsFileName, outputDir) => {
        const loggerSpy = jest.spyOn(logger, 'log').mockImplementation(() => { });
        const resultPromise = pa11yCiReporterHtml(pa11yciResultsFileName, outputDir);
        const reporterEnsureOutputSpy = jest.spyOn(reporter, 'ensureOutputDirectory').mockImplementation(() => { });
        return resultPromise.then(() => {
            expect(loggerSpy).toHaveBeenCalledWith(expect.objectContaining({
                message: expect.any(String),
                level: logger.levels.error,
                exitOnError: true,
                errorCode: 1
            }));
            expect(reporterEnsureOutputSpy).not.toHaveBeenCalled();
        });
    };

    it('should log error and exit nonzero if called with source file name that is not a string', () => {
        expect.assertions(2); // eslint-disable-line no-magic-numbers
        return verifyLogErrorAndExit(undefined, 'foo/');
    });

    it('should log error and exit nonzero if called with destination directory that is not a string', () => {
        expect.assertions(2); // eslint-disable-line no-magic-numbers
        return verifyLogErrorAndExit('pa11y-ci-results.json', undefined);
    });
});
