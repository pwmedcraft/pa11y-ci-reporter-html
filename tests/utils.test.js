'use strict';

const utils = require('../lib/utils');

// eslint-disable-next-line max-lines-per-function
describe('get file name from URL', () => {
    const verifyGetFileNameFromUrl = (url, result) => {
        const fileName = utils.getFileNameFromUrl(url);
        expect(fileName).toStrictEqual(result);
    };

    it('should remove http prefix', () => {
        expect.assertions(1);
        const url = 'http://foo.com';
        const result = 'foo-com';
        verifyGetFileNameFromUrl(url, result);
    });

    it('should remove https prefix', () => {
        expect.assertions(1);
        const url = 'https://foo.com';
        const result = 'foo-com';
        verifyGetFileNameFromUrl(url, result);
    });

    it('should not remove https in the middle of a URL', () => {
        expect.assertions(1);
        const url = 'https://foo.com/?redirect=https://bar.com';
        const result = 'foo-com-redirect-https-bar-com';
        verifyGetFileNameFromUrl(url, result);
    });

    it('should remove .html suffix', () => {
        expect.assertions(1);
        const url = 'http://foo.com/index.html';
        const result = 'foo-com-index';
        verifyGetFileNameFromUrl(url, result);
    });

    it('should not remove html in the middle of a URL', () => {
        expect.assertions(1);
        const url = 'http://foo.com/index.html?foo=bar';
        const result = 'foo-com-index-html-foo-bar';
        verifyGetFileNameFromUrl(url, result);
    });

    it('should replace all special characters with a dash', () => {
        expect.assertions(1);
        const url = '_.~!*\'();:@&=+$,/?%#[]';
        const result = '';
        verifyGetFileNameFromUrl(url, result);
    });

    it('should replace multiple special characters in a row with one dash', () => {
        expect.assertions(1);
        const url = 'http://foo.com/a_b.c~d!e*f\'g(h)i;j:k@l&m=n+o$p,q/r?s%t#u[v]w';
        const result = 'foo-com-a-b-c-d-e-f-g-h-i-j-k-l-m-n-o-p-q-r-s-t-u-v-w';
        verifyGetFileNameFromUrl(url, result);
    });

    it('should remove leading dash', () => {
        expect.assertions(1);
        const url = './foo.html';
        const result = 'foo';
        verifyGetFileNameFromUrl(url, result);
    });

    it('should remove trailing dash', () => {
        expect.assertions(1);
        const url = 'http://foo.com/';
        const result = 'foo-com';
        verifyGetFileNameFromUrl(url, result);
    });
});

